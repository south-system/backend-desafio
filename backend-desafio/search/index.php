<?php header('Access-Control-Allow-Origin: *'); 
	
	$json = array(
		array("id" => 1,"nome" => "João", "idade" => "23"),
		array("id" => 2,"nome" => "Marcela", "idade" => "87"),
		array("id" => 3,"nome" => "Carlos", "idade" => "32"),
		array("id" => 4,"nome" => "Italo", "idade" => "43"),
		array("id" => 5,"nome" => "Juca", "idade" => "54"),
		array("id" => 6,"nome" => "maria", "idade" => "21"),
		array("id" => 7,"nome" => "Lurdes", "idade" => "43"),
		array("id" => 8,"nome" => "Joana", "idade" => "54"),
		array("id" => 9,"nome" => "Mateus", "idade" => "13"),
		array("id" => 10,"nome" => "Marcelina", "idade" => "25"),
		array("id" => 11,"nome" => "Geraldo", "idade" => "53"),
		array("id" => 12,"nome" => "Luan", "idade" => "32")
	);

	if(json_encode($json)!='[]'){
		echo json_encode(array("result" => true, "data" => $json));
	}else{
		echo json_encode(["result" => false]);
	}
		
?>
