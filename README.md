# Atenção

Este backend foi criado com o intuito de simular duas chamadas básicas para resolver o desafio referente ao frontend do projeto.

As duas apis disponibilizadas são mockadas e não tem conexão com nenhum framework e banco de dados.

- https://backend-southsystem.seufavorito.com/search/
- https://backend-southsystem.seufavorito.com/login/ [POST] => [username e password]
